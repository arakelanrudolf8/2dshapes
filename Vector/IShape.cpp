#include "IShape.h"

#include <algorithm>

void Point::Move(const Vector& vector)
{
	_x += vector.GetX();
	_y += vector.GetY();
}

bool Point::ContainsPoint(const Point& p) const
{
	return (_x == p._x) && (_y == p._y);
}

bool Point::CrossSegment(const Segment& s) const
{
	return s.ContainsPoint(*this);
}

IShape* Point::Clone() const 
{
	IShape* pShape = new Point(*this);
	return pShape;
}

Point Point::operator-(const Point other) const
{
	return { _x - other._x, _y - other._x };
}

Point& Point::operator-=(const Point other)
{
	_x -= other._x;
	_y -= other._y;

	return *this;
}

void Segment::Move(const Vector& vector) 
{
	_a.Move(vector);
	_b.Move(vector);
}

bool Segment::ContainsPoint(const Point& p) const
{
	if (std::min(_a.GetX(), _b.GetX()) <= p.GetX() && p.GetX() <= std::max(_a.GetX(), _b.GetX()) &&
		std::min(_a.GetY(), _b.GetY()) <= p.GetY() && p.GetY() <= std::max(_a.GetY(), _b.GetY()))
	{
		Line line(_a, _b);
		return line.GetA() * p.GetX() + line.GetB() * p.GetY() + line.GetC() == 0;
	}

	return false;
}

inline int64_t Area(Point a, Point b, Point c) {
	return (b.GetX() - a.GetX()) * (c.GetY() - a.GetY()) - (b.GetY() - a.GetY()) * (c.GetX() - a.GetX());
}

inline bool Intersect(int64_t a, int64_t b, int64_t c, int64_t d) {
	if (a > b) std::swap(a, b);
	if (c > d) std::swap(c, d);
	return std::max(a, c) <= std::min(b, d);
}

bool Segment::CrossSegment(const Segment& s) const
{
	return Intersect(_a.GetX(), _b.GetX(), s._a.GetX(), s._b.GetX())
		&& Intersect(_a.GetY(), _b.GetY(), s._a.GetY(), s._b.GetY())
		&& Area(_a, _b, s._a) * Area(_a, _b, s._b) <= 0
		&& Area(s._a, s._b, _a) * Area(s._a, s._b, _b) <= 0;
}

IShape* Segment::Clone() const
{
	IShape* pShape = new Segment(*this);
	return pShape;
}

void Line::Move(const Vector& vector)
{
	_c += _a * vector.GetX() + _b * vector.GetY();
}

bool Line::ContainsPoint(const Point& p) const
{
	return GetA() * p.GetX() + GetB() * p.GetY() + GetC() == 0;
}

bool Line::CrossSegment(const Segment& s) const
{
	return (_a * s.GetA().GetX() + _b * s.GetA().GetY() + _c) *
		(_a * s.GetB().GetX() + _b * s.GetB().GetY() + _c) <= 0;
}

IShape* Line::Clone() const
{
	IShape* pShape = new Line(*this);
	return pShape;
}

void Ray::Move(const Vector& vector)
{
	_a.Move(vector);
}

bool Ray::ContainsPoint(const Point& p) const
{
	bool inRange = false;
	if (_vector.GetX() > 0 && _vector.GetY() > 0)
		inRange = _a.GetX() <= p.GetX() && _a.GetX() <= p.GetX();
	else if (_vector.GetX() > 0 && _vector.GetY() < 0)
		inRange = _a.GetX() <= p.GetX() && _a.GetX() >= p.GetX();
	else if (_vector.GetX() < 0 && _vector.GetY() < 0)
		inRange = _a.GetX() >= p.GetX() && _a.GetX() >= p.GetX();
	else if (_vector.GetX() < 0 && _vector.GetY() > 0)
		inRange = _a.GetX() >= p.GetX() && _a.GetX() <= p.GetX();

	if (inRange)
	{
		Point _b = { _vector.GetX() + _a.GetX(), _vector.GetY() + _a.GetY() };
		Line line(_a, _b);
		return line.GetA() * p.GetX() + line.GetB() * p.GetY() + line.GetC() == 0;
	}

	return false;
}

bool Ray::CrossSegment(const Segment& s) const
{
	Point _b = { _vector.GetX() + _a.GetX(), _vector.GetY() + _a.GetY() };
	Line line1(_a,_b);
	auto val1 = line1.GetA() * s.GetA().GetX() + line1.GetB() * s.GetA().GetY() + line1.GetC();
	auto val2 = line1.GetA() * s.GetB().GetX() + line1.GetB() * s.GetB().GetY() + line1.GetC();
	if (val1 * val2 <= 0)
	{
		Line line2(s.GetA(), s.GetB());
		Point lineInstersect;
		if (!val1)
			lineInstersect = s.GetA();
		else if (!val2)
			lineInstersect = s.GetB();
		else if (!val1 && !val2)
		{
			if (s.ContainsPoint(_a))
				return true;
			else
				lineInstersect = s.GetA();
		}
		else
			lineInstersect = { (line1.GetB() * line2.GetC() - line1.GetB() * line1.GetC() * line2.GetA() /
				line2.GetA()) / (line2.GetB() * line1.GetA() - line2.GetA() * line1.GetB()) - line1.GetC() / line1.GetA(),
				(line1.GetC() * line2.GetA() / line1.GetA() - line2.GetC()) / (line2.GetB() - line2.GetA() * line1.GetB() / line1.GetA()) };

		if ((lineInstersect.GetX() - _a.GetX()) * _vector.GetX() >= 0 &&
			(lineInstersect.GetY() - _a.GetY()) * _vector.GetY() >= 0)
			return true;
	}

	return false;
}

IShape* Ray::Clone() const
{
	IShape* pShape = new Ray(*this);
	return pShape;
}


void Circle::Move(const Vector& vector)
{
	_center.Move(vector);
}

bool Circle::ContainsPoint(const Point& p) const
{
	return pow(_center.GetX() - p.GetX(), 2) + pow(_center.GetY() - p.GetY(), 2) == pow(_radius, 2);
}

bool Circle::CrossSegment(const Segment& s) const
{
	return (pow(_center.GetX() - s.GetA().GetX(), 2) + pow(_center.GetY() - s.GetA().GetY(), 2) - pow(_radius, 2)) *
		(pow(_center.GetX() - s.GetB().GetX(), 2) + pow(_center.GetY() - s.GetB().GetY(), 2) - pow(_radius, 2)) <= 0;
}

IShape* Circle::Clone() const
{
	IShape* pShape = new Circle(*this);
	return pShape;
}
