#include "Vector.h"

#include <cmath>

Vector::Vector(int64_t x, int64_t y) :
	_x(x),
	_y(y)
{};

int64_t Vector::operator*(const Vector& other) const
{
	return _x * other._x + _y * other._y;
}

int64_t Vector::operator^(const Vector& other) const
{
	return abs(_x * other._y - _y * other._x);
}

Vector Vector::operator+(const Vector& other) const
{
	return Vector(_x + other._x, _y + other._y);
}

Vector& Vector::operator+=(const Vector& other)
{
	_x += other._x;
	_y += other._y;

	return *this;
}

Vector Vector::operator-(const Vector& other) const
{
	return Vector(_x - other._x, _y - other._y);
}

Vector& Vector::operator-=(const Vector& other)
{
	_x -= other._x;
	_y -= other._y;

	return *this;
}

Vector Vector::operator*(int64_t value) const
{
	return Vector(_x * value, _y * value);
}

Vector& Vector::operator*=(int64_t value)
{
	_x *= value;
	_y *= value;

	return *this;
}

Vector& Vector::operator-() 
{
	_x = -_x;
	_y = -_y;

	return *this;
}

Vector operator*(int64_t value, const Vector& v)
{
	return v * value;
}