#pragma once

#include <cstdint>

class Vector
{
public:
	Vector() = default;
	Vector(int64_t, int64_t);

	int64_t operator*(const Vector& other) const;
	int64_t operator^(const Vector& other) const;

	Vector operator-(const Vector& other) const;
	Vector& operator-=(const Vector& other);

	Vector operator+(const Vector& other) const;
	Vector& operator+=(const Vector& other);

	Vector operator*(int64_t value) const;
	Vector& operator*=(int64_t value);

	Vector& operator-();

	inline int64_t GetX() const { return _x; };
	inline int64_t GetY() const { return _y; };

private:
	int64_t _x = 0;
	int64_t _y = 0;
};

Vector operator*(int64_t value, const Vector& v);
