#include "Vector.h"
#include "IShape.h"
#include <iostream>

int main()
{
	Ray ray({ 1, 1 }, { 3, 2 });
	Segment s({ 4, 3 }, { 1, 1 });

	std::cout << ray.CrossSegment(s);
}