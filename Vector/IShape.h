#pragma once

#include "Vector.h"

#include <cstdint>

class Point;
class Segment;
class Line;
class Ray;
class Circle;

class IShape
{
public:
	virtual void Move(const Vector&) = 0;
	virtual bool ContainsPoint(const Point&) const = 0;
	virtual bool CrossSegment(const Segment&) const = 0;
	virtual IShape* Clone() const = 0;
};

class Point : public IShape
{
public:
	Point() = default;
	Point(int64_t x, int64_t y) : _x(x), _y(y) {};

	int64_t GetX() const { return _x; };
	int64_t GetY() const { return _y; };

	void Move(const Vector&) override;
	bool ContainsPoint(const Point&) const override;
	bool CrossSegment(const Segment&) const override;
	IShape* Clone() const override;

	Point operator-(const Point other) const;
	Point& operator-=(const Point other);

private:
	int64_t _x = 0;
	int64_t _y = 0;
};

class Segment : public IShape
{
public:
	Segment() = default;
	Segment(Point a, Point b) : _a(a), _b(b) {};

	Point GetA() const { return _a; };
	Point GetB() const { return _b; };

	void Move(const Vector&) override;
	bool ContainsPoint(const Point&) const override;
	bool CrossSegment(const Segment&) const override;
	IShape* Clone() const override;

private:
	Point _a;
	Point _b;
};

class Line : public IShape
{
public:
	Line() = default;
	Line(Point a, Point b) 
	{
		_a = b.GetY() - a.GetY();
		_b = a.GetX() - b.GetX();
		_c = a.GetY() * b.GetX() - a.GetX() * b.GetY();
	};

	int64_t GetA() const { return _a; };
	int64_t GetB() const { return _b; };
	int64_t GetC() const { return _c; };

	void Move(const Vector&) override;
	bool ContainsPoint(const Point&) const override;
	bool CrossSegment(const Segment&) const override;
	IShape* Clone() const override;

private:
	int64_t _a;
	int64_t _b;
	int64_t _c;
};

class Ray : public IShape
{
public:
	Ray() = default;
	Ray(Point a, Point b) : _a(a), _vector(Vector(b.GetX(), b.GetY())) {};

	Point GetA() const { return _a; };
	Vector GetVector() const { return _vector; };

	void Move(const Vector&) override;
	bool ContainsPoint(const Point&) const override;
	bool CrossSegment(const Segment&) const override;
	IShape* Clone() const override;

private:
	Point _a;
	Vector _vector;
};

class Circle : public IShape
{
public:
	Circle() = default;
	Circle(Point center, size_t radius) : _center(center), _radius(radius) {};

	Point GetCenter() const { return _center; };
	size_t GetRadius() const { return _radius; };

	void Move(const Vector&) override;
	bool ContainsPoint(const Point&) const override;
	bool CrossSegment(const Segment&) const override;
	IShape* Clone() const override;

private:
	Point _center;
	size_t _radius;
};